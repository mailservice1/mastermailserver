<?php declare(strict_types = 1);


/**
 * Balance server name
 */
define("REPORT_BACK_TO","balanceServer");

/**
 * Balance server ip address
 */
define("REPORT_BACK_IP","192.168.0.38");

/**
 * Balance server port
 */
define("RB_PORT", 5533);

/**
 * Balance target script
 */
define("RB_TARGET", "report.php");

# End definitions

define('ROOT', $_SERVER['DOCUMENT_ROOT'].'/');

require ROOT . 'vendor/autoload.php';

use Mailservice\Node\Core\ApiShell as API;
use Mailservice\Node\Core\Api\IMethod as METHOD;
use Mailservice\Node\Controller;
use Mailservice\Node\Utility\Exception\ApiException;

use Mailservice\Node\Contracts\Report;
use Mailservice\Node\Contracts\Settings\Server;

try
{   
    switch ( API::method() ) 
    {   
        case METHOD::GET: Controller::healthCheck(); break;

        case METHOD::POST: Controller::manageCycleRequest( API::incomeing() ); break;                    
                            
    
        default: Controller::methodNotAllowed(); break;
    }
    
    /**
     * Return response !
     */
    http_response_code( Controller::getResponseCode() );
    header('Content-Type: '. Controller::$responseContent);
    header("Node-response: " . Controller::$responseHeader);
    
    Controller::response();
    
}
catch( ApiException $e )
{  
    header("Node-response: " . $e->getMessage());
    header('Content-Type: none');
    http_response_code( Controller::getResponseCode() );
}




?>