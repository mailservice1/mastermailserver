<?php

namespace Mailservice\Node\Utility;

use Mailservice\Node\Contracts\Settings\Server;

abstract class Curl 
{   
    private static array $headers = ['Content-Type: application/json'];

    private static $init;

    private static int $responseCode;

    private static mixed $body = null;

    private static bool $ssl = false;

    private Server $serverSettings;

    public function __construct( Server $_serverSettings )
    {
        $this->serverSettings = $_serverSettings;
    }

    /**
     * 
     * @access public 
     * @method connect
     * @var Server serversettings 
     * @return CurlResponse
     * @static
     */
    public static function connect( Server $_serverSettings ): CurlResponse
    {   
        self::url($_serverSettings);

        self::ssl();

        if( self::$body != null ) self::postBody();
       
        $response = self::execute();
        
        if(curl_errno(self::$init)) throw new CurlException( curl_error(self::$init) );
           
        $fullCurlResponse =  new CurlResponse(
            (int)curl_getinfo(self::$init, CURLINFO_HTTP_CODE),
            curl_getinfo(self::$init, CURLINFO_CONTENT_TYPE),
            $response 
        );

        curl_close(self::$init);

        return $fullCurlResponse;      
    }

    /**
     * @method
     * 
     */
    public function post( mixed $_body ): CurlResponse
    {   
        self::$body = $_body;

        return self::connect( $this->serverSettings);
    }

    /**
     * @access private
     * @method url
     * @var Server
     */
    private static function url( Server $_serverSettings ): void
    {   
        
        if($_serverSettings->targetFile == null)
        {
            $setUrl = "http://" . $_serverSettings->serverIp . ":" . $_serverSettings->serverPort;
        }else  $setUrl = "http://" . $_serverSettings->serverIp . ":" . $_serverSettings->serverPort . "/" . $_serverSettings->targetFile;
        
        self::$init = $initiliseCurl = curl_init();
        curl_setopt(self::$init, CURLOPT_URL, $setUrl);
        //curl_setopt(self::$init, CURLOPT_HEADER, TRUE);
        curl_setopt(self::$init, CURLOPT_RETURNTRANSFER, TRUE);
    }

    private static function ssl(): void
    {
        curl_setopt(self::$init, CURLOPT_SSL_VERIFYPEER, self::$ssl );
    }

    private static function postBody(): void
    {
        curl_setopt(self::$init, CURLOPT_POST, true);
        
        curl_setopt(self::$init, CURLOPT_POSTFIELDS, self::$body ); 
    }

    private static function execute()
    {
        curl_setopt(self::$init, CURLOPT_HTTPHEADER, self::$headers);  

        return curl_exec( self::$init );
    }

    
}

class CurlResponse 
{   
    public int $responseCode;

    public string $responseCotentType;

    public string|object|null $body;


    public function __construct(
        int $_response_code,
        string $_respone_content_type,
        string|object $_body = null
    )
    {
        $this->responseCode = $_response_code;
        $this->responseCotentType = $_respone_content_type;
        $this->body = $_body;
    }
}
class CurlException extends \Exception
{

}