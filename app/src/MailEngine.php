<?php

namespace Mailservice\Node;

use Mailservice\Node\Contracts\Subcycle;
use Mailservice\Node\Contracts\Report;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

use Mailservice\Node\Contracts\Cluster\SubCycle\SmtpSettings;


class MailEngine 
{   
    private Report $cycleReport;

    private PHPMailer $mailer;


    public function __construct( SmtpSettings $_smtp_settigns )
    {   
        $this->mailer = new PHPMailer(true);

        $this->mailer->isSMTP();

        $this->mailer->Host = $_smtp_settigns->host;

        $this->mailer->Port = $_smtp_settigns->port;

        $this->mailer->SMTPAuth = true;

        $this->mailer->CharSet = "UTF-8";
        
        $this->mailer->Username = $_smtp_settigns->username;

        $this->mailer->Password = $_smtp_settigns->password;

        $this->mailer->SMTPSecure = $_smtp_settigns->secure;

        #$this->mailer->addReplyTo($_smtp_settigns->replyTo);
        

    }
    public function sendEmail( ): true|string
    {
        /*
        $this->mailer->addAddress($email->toEmail);
        $this->mailer->Subject = $email->subject;
        $this->mailer->isHTML(($email->isHtml == 1) ? true : false);
        $this->mailer->Body = stripslashes($email->emailBody);
        */
            try
            {
                #$this->mailer->send();
                $this->mailer->clearAddresses();
                
                return true;

            }catch(Exception $e)
            {
                return false;
            }
    }
    
}