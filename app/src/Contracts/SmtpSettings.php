<?php

namespace Mailservice\Node\Contracts;

class SmtpSettings 
{   
    public string $host; 

    public int $port;

    public function __construct( mixed $_smpt_settings )
    {
        $this->host = $_smpt_settings->host;

        $this->port = $_smpt_settings->port;

    }
}