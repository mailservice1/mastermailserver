<?php

namespace Mailservice\Node\Contracts\Cluster\SubCycle;

readonly class SmtpSettings 
{
    public string $host;

    public int $port;

    public string $username;

    public string $password;

    public string $sender;

    public string $replyTo;

    public string $sslIv;

    public bool $secure;



    public function __construct(

        string $_host,
        int $_port,
        string $_username,
        string $_password,
        string $_sender,
        string $_reply_to,
        string $_sslIv,
        bool $_secure

    ){
        $this->host = $_host;
        $this->port = $_port;
        $this->username = $_username;
        $this->password = $_password;
        $this->sender = $_sender;
        $this->replyTo = $_reply_to;
        $this->sslIv = $_sslIv;
        $this->secure = $_secure;
    }
}