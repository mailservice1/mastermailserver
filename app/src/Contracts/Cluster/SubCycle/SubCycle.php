<?php

namespace Mailservice\Node\Contracts\Cluster\SubCycle;

class SubCycle
{
    public SubCycleHead $subCycleHead;

    public SmtpSettings $smtpSettings;

    public array $emailQueue;

    public function __construct(

        SubCycleHead $_subCycleHead,
        SmtpSettings $_smtpSettings,
        array $_email_queue

    ){
        $this->subCycleHead = $_subCycleHead;

        $this->smtpSettings = $_smtpSettings;

        $this->emailQueue = $_email_queue;
    }
}