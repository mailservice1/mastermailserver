<?php

namespace Mailservice\Node\Contracts\Cluster;

use Mailservice\Node\Utility\Curl;
use Mailservice\Node\Contracts\Settings\Server;
use Mailservice\Node\Contracts\Cluster\SubCycle\SubCycleHead;


/**
 * Report back to balance server.
 * 
 * 
 * 
 */
class Report extends Curl
{   
    public SubCycleHead $subCycleHead;

    public array $emailReport = [];

    public string $utility;

    public function __construct(

        Server $_server,
        SubCycleHead $_sub_cycle_head,
        string $_utility

    )
    {   
        parent::__construct($_server);
        $this->subCycleHead = $_sub_cycle_head;
        $this->utility = $_utility;
    }

    /**
     * @method addLine
     * @var ReportLine
     * 
     * Add report line to existing report list.
     * 
     */
    public function addLine( ReportLine $_report_line ): void
    {
        array_push($this->emailReport, $_report_line);
    }

    /**
     * @method send
     * 
     * Exsecute sending of each subcycle report.
     */
    public function send()
    {   $result = $this->post( json_encode($this) );
        var_dump($result->body);
        return $result;
    }
}

class ReportLine
{   
    public int $emailId;

    public bool|int $timestamp;


    public function __construct( int $_mail_id, bool|int $_sendTimeStamp = false )
    {
        $this->emailId = $_mail_id;

        $this->timestamp = $_sendTimeStamp;

    }
}