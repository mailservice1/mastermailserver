<?php

namespace Mailservice\Node\Contracts\Settings;

readonly class Server 
{
    public string $serverId;

    public string $serverIp;

    public int $serverPort;

    public null|string $serverCypher;

    public null|string $targetFile;

    public function __construct(

        string $_serverId,
        string $_serverIp,
        int $_serverPort,
        null|string $_serverCypher = null,
        null|string $_targetFile = null
        
    ){
        $this->serverId = $_serverId;
        $this->serverIp = $_serverIp;
        $this->serverPort = $_serverPort;
        $this->serverCypher = $_serverCypher;
        $this->targetFile = $_targetFile;
    }
}