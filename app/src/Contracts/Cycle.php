<?php

namespace Mailservice\Node\Contracts;

class Cycle 
{   
    public string $cycleId;

    public static array $subCycles = [];

    public function __construct( mixed $_incomeing_cycle )
    {
        $this->cycleId = $_incomeing_cycle->head->cycleId;
        self::addSubCycle($_incomeing_cycle);
        
    }
    private static function addSubCycle( mixed $_incomeing_cycle ): void
    {   
        array_push(self::$subCycles,array(
            $_incomeing_cycle->head->subcycleId => new Subcycle($_incomeing_cycle)));

    }
    public static function add( mixed $_incomeing_cycle ): void
    {
        self::addSubCycle( $_incomeing_cycle );
    }
    public static function subCycles(): array
    {
        return self::$subCycles;
    }
}
