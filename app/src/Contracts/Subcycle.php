<?php

namespace Mailservice\Node\Contracts;

class Subcycle 
{   
    public string $cycleId;

    public string $subCycleId;

    public int $waitingEmails;

    public SmtpSettings $smtpSettings;

    public function __construct( mixed $_incomeing_cycle )
    {
        $this->cycleId = $_incomeing_cycle->head->cycleId;

        $this->subCycleId = $_incomeing_cycle->head->subcycleId;

        $this->waitingEmails = (int)$_incomeing_cycle->head->waitingEmailCount;

        $this->smtpSettings = new SmtpSettings( $_incomeing_cycle->smptSettings );
    }
}