<?php

namespace Mailservice\Node;

use Mailservice\Node\Core\ApiShell;
use Mailservice\Node\Contracts\Cycle;
use Mailservice\Node\Contracts\Cluster\Report;
use Mailservice\Node\Contracts\Cluster\ReportLine;
use Mailservice\Node\Utility\Exception\ApiException;

use Mailservice\Node\MailEngine;

use Mailservice\Node\Contracts\Settings\Server;

use Mailservice\Node\Contracts\Cluster\SubCycle\SubCycle;
use Mailservice\Node\Contracts\Cluster\SubCycle\SmtpSettings;
use Mailservice\Node\Contracts\Cluster\SubCycle\SubCycleHead;

/**
 * 
 */
class Controller extends ApiShell
{   
    public static string $responseContent = "none";

    public static string $responseHeader = "healtcheck OK";

    /**
     * @method healthCheck
     * Return ping to balance server <OK:200></OK:200>
     * 
     */
    public static function healthCheck()
    {
        self::code(200);
    }

    /**
     * 
     * @method manageCycleRequest
     * @var mixed incomeing
     * Interceps settings, rewrite subcycle head, collects
     * reports from each sended email and reports back 
     * to balance server.
     * 
     */
    public static function manageCycleRequest( mixed $_incomeing )
    {   
        try
        {   
            /**
             * // TODO: - optional data ckecking
             * @var SmtpSettings
             */
            $smtpSettings = new SmtpSettings(
                $_incomeing->smtpSettings->host,
                $_incomeing->smtpSettings->port,
                $_incomeing->smtpSettings->username,
                $_incomeing->smtpSettings->password,
                $_incomeing->smtpSettings->sender,
                $_incomeing->smtpSettings->replyTo,
                $_incomeing->smtpSettings->sslIv,
                $_incomeing->smtpSettings->secure
            );
            

            
            $report = new Report(
                new Server(REPORT_BACK_TO,REPORT_BACK_IP, RB_PORT, null, RB_TARGET),
                new SubCycleHead(
                    $_incomeing->head->cycleId,
                    $_incomeing->head->cycleTs,
                    $_incomeing->head->subcycleId,
                    $_incomeing->head->waitingEmailCount
                ),
                $_incomeing->utility
            );
               
            $mailEngine = new MailEngine( $smtpSettings );

            foreach( $_incomeing->emailList as $emailPacage )
            {
                $emailSequence = $mailEngine->sendEmail( $emailPacage );
                if($emailSequence !== true) $report->addLine( new ReportLine( (int)$emailPacage->id, false) );
                    else $report->addLine( new ReportLine( (int)$emailPacage->id, time() ) );
            }
                    
            /**
             * Send report back to report intercepter!
             */
            $report->send();
            self::code(200);
            
        }
        catch(Exception $e)
        {
            throw new ApiException($e->getMessage);
        }    
    }  
}