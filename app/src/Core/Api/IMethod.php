<?php

namespace Mailservice\Node\Core\Api;

interface IMethod 
{
    public const GET = 'GET';

    public const POST = 'POST';

}