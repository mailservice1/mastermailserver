<?php

namespace Mailservice\Node\Core;


use Mailservice\Node\Contracts\Cycle;
use Mailservice\Node\Contracts\Subcycle;
use Mailservice\Node\Utility\Exception\ApiException;


class ApiShell 
{   
    protected static int $responseCode = 500;

    protected static mixed $response = null;


    public static function response(): void 
    {   
        if( self::$response !== null)
            echo json_encode( self::$response );
    }


    public static function method(): string
    {
        return $_SERVER['REQUEST_METHOD'];
    }
    
    public static function incomeing()
    {
        if(file_get_contents('php://input'))
            {
                return json_decode(file_get_contents('php://input'));
            }
    }

    public static function getResponseCode(): int 
    {
        return self::$responseCode;
    }

    public static function methodNotAllowed()
    {
        self::code(405);
    }

    protected static function code( int $_response_code ): void 
    {
        self::$responseCode = $_response_code;
    }
    
    
}